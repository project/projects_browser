<?php

namespace Drupal\projects_browser\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\projects_browser\ProjectBrowserTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Provides controller for ssh with composer.
 */
class SSHComposer extends ControllerBase {
  use ProjectBrowserTrait;
  use MessengerTrait;

  /**
   *
   */
  public function browseProjectSsh($project_machine_name, $version = '') {
    $module_status = $this->getModuleStatusList();
    if (!isset($module_status[$project_machine_name])) {
      $project = 'drupal/' . $project_machine_name;
      $ssh = $this->getSshClient();
      if (is_null($ssh)) {
        $url = Url::fromRoute('projects_browser.ssh_settings')->toString();
        $response = new RedirectResponse($url);
        $response->send();
      }
      if ($version != '' and $version != 'default') {
        $version_parts = explode('-', $version);
        if (isset($version_parts[0])) {
          $version_parts1 = explode('.', $version_parts[0]);
          $version_parts[0] = $version_parts1[0] . '.' . $version_parts1[1];
        }
        if (isset($version_parts[1])) {
          if (strpos($version_parts[1], 'alpha') !== FALSE) {
            $version_parts[1] = 'alpha';
          }
          if (strpos($version_parts[1], 'beta') !== FALSE) {
            $version_parts[1] = 'beta';
          }
        }
        $version = implode('@', $version_parts);
        $project .= ':^' . $version;
      }
      $this->requireProject($project);
      $url = Url::fromRoute('system.modules_list')->toString();
    }
    else {
      $message = 'This module ' . $module_status[$project_machine_name]['name'] . '  is already browsed.';
      if ($module_status[$project_machine_name]['status'] == 1) {
        $message .= 'And also installed.';
        $url = Url::fromRoute('projects_browser.browse_modules')->toString();
      }
      else {
        $url = Url::fromRoute('system.modules_list')->toString();
      }
      $this->messenger()->addWarning($message);
    }
    $response = new RedirectResponse($url);
    $response->send();
  }

  /**
   *
   */
  public function removeProjectSsh($project_machine_name) {
    $module_status = $this->getModuleStatusList();
    if (isset($module_status[$project_machine_name])) {
    	if ($module_status[$project_machine_name]['status'] == 0) {
	      $project = 'drupal/' . $project_machine_name;
	      $ssh = $this->getSshClient();
	      if (is_null($ssh)) {
	        $url = Url::fromRoute('projects_browser.ssh_settings')->toString();
	        $response = new RedirectResponse($url);
	        $response->send();
	      }
	    	$message = 'This module ' . $project_machine_name . '  is removed from your site.';
        $this->removeProject($project);
	    }
	    else{
	    	$message = 'This module ' . $project_machine_name . '  is installed. so, could not removed from your site.';
	    }
      $this->messenger()->addWarning($message);
      $url = Url::fromRoute('system.modules_list')->toString();
    }
    else {
      $message = 'This module ' . $project_machine_name . '  is not available in your site.';
      $url = Url::fromRoute('system.modules_list')->toString();
      $this->messenger()->addWarning($message);
    }
    $response = new RedirectResponse($url);
    $response->send();
  }
  /**
   *
   */
  public function requireProject($package) {
    $ssh = $this->getSshClient();
    if (!is_null($ssh)) {
      $config = \Drupal::config('project-browser-ssh.settings');
      $composerLog = $config->get('composer_log');
      $reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
      $composerPath = dirname(dirname(dirname($reflection->getFileName())));
      $comment = 'cd ' . $composerPath . ';';
      $comment .= 'composer require "' . $package . '"';
      $stream = ssh2_exec($ssh->connection, $comment);
      stream_set_blocking($stream, TRUE);
      $stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
      $stream_out_err = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
      if($composerLog) {
        stream_set_blocking($stream, TRUE);
        $stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
        $stream_out_err = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
        $stream_content = '<pre>' . stream_get_contents($stream_out) . '</pre>' ;
        $stream_content_err = '<pre>' . stream_get_contents($stream_out_err) . '</pre>' ;
        $this->messenger()->addMessage($this->t($stream_content));
        $this->messenger()->addMessage($this->t($stream_content_err)); 
      }
      else {
        $this->messenger()->addMessage($this->t('Project downloaded by composer.'));
      }
    }
  }


  public function removeProject($package) {
    $ssh = $this->getSshClient();
    if (!is_null($ssh)) {
      $config = \Drupal::config('project-browser-ssh.settings');
      $composerLog = $config->get('composer_log');
      $reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
      $composerPath = dirname(dirname(dirname($reflection->getFileName())));
      $comment = 'cd ' . $composerPath . ';';
      $comment .= 'composer remove "' . $package . '"';
      $stream = ssh2_exec($ssh->connection, $comment);
      if($composerLog) {
        stream_set_blocking($stream, TRUE);
        $stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
        $stream_out_err = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
        $stream_content = '<pre>' . stream_get_contents($stream_out) . '</pre>' ;
        $stream_content_err = '<pre>' . stream_get_contents($stream_out_err) . '</pre>' ;
        $this->messenger()->addMessage($this->t($stream_content));
        $this->messenger()->addMessage($this->t($stream_content_err)); 
      }
    }
  }

  /**
   *
   */
  public function composerUpdate() {
    $ssh = $this->getSshClient();
    if (!is_null($ssh)) {
      $comment = 'cd ' . DRUPAL_ROOT . ';';
      $comment .= 'composer update';
      $stream = ssh2_exec($ssh->connection, $comment);
      stream_set_blocking($stream, TRUE);
      $stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
      $stream_out_err = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
      $stream_content = stream_get_contents($stream_out);
      $stream_content_err = stream_get_contents($stream_out_err);
      $return = [
        '#theme' => 'container',
        '#children' => [
          '#markup' => '<pre>' . $stream_content . '<br>' . $stream_content_err . '</pre>',
        ],
      ];
    }
    else {
        $url = Url::fromRoute('system.modules_list')->toString();
		    $response = new RedirectResponse($url);
		    $response->send();
    }
  }
}
