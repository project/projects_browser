<?php

namespace Drupal\projects_browser\Controller;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use GuzzleHttp\Exception\RequestException;

/**
 * Fetches project information from remote locations.
 */
class ProjectUpdateFetcher {

  use DependencySerializationTrait;

  /**
   * The update settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $updateSettings;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Whether to use HTTP fallback if HTTPS fails.
   *
   * @var bool
   */
  protected $withHttpFallback;

  /**
   * Constructs an UpdateFetcher.
   */
  public function __construct() {
    $settings = \Drupal::service('settings');
    $this->withHttpFallback = $settings->get('update_fetch_with_http_fallback', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchProjectData($project_name) {
    $url = 'https://updates.drupal.org/release-history/' . $project_name . '/current';
    ;
    return $this->doRequest($url, ['headers' => ['Accept' => 'text/xml']], $this->withHttpFallback);
  }

  /**
   * Applies a GET request with a possible HTTP fallback.
   *
   * This method falls back to HTTP in case there was some certificate
   * problem.
   *
   * @param string $url
   *   The URL.
   * @param array $options
   *   The guzzle client options.
   * @param bool $with_http_fallback
   *   Should the function fall back to HTTP.
   *
   * @return string
   *   The body of the HTTP(S) request, or an empty string on failure.
   */
  protected function doRequest(string $url, array $options, bool $with_http_fallback): string {
    $data = '';
    try {
      $data = (string) \Drupal::httpClient()->get($url, ['headers' => ['Accept' => 'text/xml']])->getBody();
    }
    catch (RequestException $exception) {
      watchdog_exception('update', $exception);
      if ($with_http_fallback && strpos($url, "http://") === FALSE) {
        $url = str_replace('https://', 'http://', $url);
        return $this->doRequest($url, $options, FALSE);
      }
    }
    return $data;
  }

}
