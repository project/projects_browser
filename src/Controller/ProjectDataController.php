<?php

namespace Drupal\projects_browser\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\projects_browser\ProjectBrowserTrait;
use Drupal\Core\Messenger\MessengerTrait;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;

/**
 * Provides controller for projects data.
 */
class ProjectDataController extends ControllerBase {
  use ProjectBrowserTrait;
  use MessengerTrait;
  /**
   * Whether to use HTTP fallback if HTTPS fails.
   *
   * @var bool
   */
  protected $updateFetcher;

  /**
   * Constructs an UpdateFetcher.
   */
  public function __construct() {
    $this->updateFetcher = new ProjectUpdateFetcher();
  }

  /**
   *
   */
  public function getProjects($filters) {
    $url = 'https://www.drupal.org/api-d7/node.json';
    $filter_keys = [
      'type', 'field_project_machine_name',
      'field_security_advisory_coverage', 'taxonomy_vocabulary_44',
      'taxonomy_vocabulary_46', 'taxonomy_vocabulary_3', 'sort', 'direction',
    ];
    $option = [
      'query' => [
        'limit' => 20,
        'field_project_type' => 'full',
      ],
    ];
    foreach ($filters as $key => $value) {
      if (in_array($key, $filter_keys)) {
        $option['query'][$key] = $value;
      }
    }
    if (isset($filters['pager'])) {
      $key_pairs = [
        'First' => 'first',
        'Next' => 'next',
        'Previous' => 'prev',
        'Last' => 'last',
      ];
      $pager_id = $key_pairs[$filters['pager']];
      $option['query']['page'] = $_SESSION[$pager_id];
    }
    elseif (isset($filters['pager_self']) && isset($_SESSION['self'])) {
      if (((int) $filters['pager_self'] != (int) $_SESSION['self']) && ((int) $filters['pager_self'] <= (int) $_SESSION['last'])) {
        $option['query']['page'] = $filters['pager_self'];
      }
      elseif (isset($filters['pager'])) {
        $key_pairs = [
          'First' => 'first',
          'Next' => 'next',
          'Previous' => 'prev',
          'Last' => 'last',
        ];
        $pager_id = $key_pairs[$filters['pager']];
        $option['query']['page'] = $_SESSION[$pager_id];
      }
      else {
        $option['query']['page'] = 0;
      }
    }
    else {
      $option['query']['page'] = 0;
    }
    $project_url = Url::fromUri($url, $option);
    $project_url = $project_url->toString();
    $project_data = $this->getData($project_url);
    if (isset($project_data['ClientException'])) {
      $option['query']['page'] = 0;
      $project_url = Url::fromUri($url, $option);
      $project_url = $project_url->toString();
      $project_data = $this->getData($project_url);
    }
    if (isset($project_data['network_issue'])) {
      $data = [
        'content' => [
          '#markup' => '<div class="network-issue">' . $this->t($project_data['network_issue']) . '</div>',
        ],
      ];
    }
    else {
      if (count($project_data['list']) > 0) {
        $project_list_items = $this->getProjectListItems($project_data['list']);
        $project_list = [
          '#theme' => 'project_list',
          '#project_items' => $project_list_items,
        ];
        $pager_lists = ['self', 'first', 'prev', 'next', 'last'];
        $pager = [];
        foreach ($pager_lists as $pager_id) {
          $pager_data = $this->getUrlParameters($project_data[$pager_id]);
          $_SESSION[$pager_id] = $pager_data['page'];
          $pager[$pager_id] = $pager_data['page'];
        }
        $data = [
          'content' => $project_list,
          'pager' => $pager,
        ];
      }
      else {
        $data = [
          'content' => [
            '#markup' => '<div class="no-results">' . $this->t('No results found.') . '</div>',
          ],
        ];
      }
    }
    return $data;
  }

  /**
   *
   */
  public function getData($url) {
    try {
      $client = \Drupal::httpClient();
      $response = $client->get($url);
      $response = $response->getBody();
      $data = json_decode($response, TRUE);
    }
    catch (ConnectException $e) {
      $data['network_issue'] = 'Projects browser needs internet connection. Check your network.';
    }
    catch (ClientException $e) {
      $data['ClientException'] = '404';
    }
    catch (Exception $e) {
      $data['network_issue'] = $e->getMessage();
    }
    return $data;
  }

  /**
   *
   */
  public function getProjectListItems($projectLists) {
    $projects = [];
    $projectStates = [
      0 => 'Install Now',
      1 => 'Installed',
      2 => 'Download Now',
      3 => 'Unsupported',
    ];
    $available_modules = $this->getModuleStatusList();
    $terms = $this->getProjectBrowserTerms();
    foreach ($projectLists as $key => $project) {
      $projectName = $project['field_project_machine_name'];
      if (count($project['field_project_images'])) {
        $projectImages = [];
        foreach ($project['field_project_images'] as $key => $value) {
          $imageData = $this->getData($value['file']['uri'] . '.json');
          $projectImages[] = [
            'url' => $imageData['url'],
            'alt' => $value['alt'],
          ];
        }
        $project['field_project_images'] = $projectImages;
      }
      if (count($project['flag_project_star_user']) > 0) {
        $project['flag_project_star_user_count'] = count($project['flag_project_star_user']);
      }
      if(isset($project['project_usage'])){
        if (is_array($project['project_usage']) && count($project['project_usage']) > 0) {
          $project['project_usage_sum'] = array_sum($project['project_usage']);
        } 
      }
      if (!isset($available_modules[$projectName])) {
        $project_support = $this->getProjectStatusData($projectName);
        if (!is_null($project_support)) {
          $project['project_support'] = $project_support;
          $project['project_support']['status'] = 2;
          $version = 'default';
          if (isset($project_support['compatible_versions'])) {
            $version = $project_support['compatible_versions'];
          }
          $params = [
            'project_machine_name' => $projectName,
            'version' => $version,
          ];
          $url = Url::fromRoute('projects_browser.browse_project', $params);
          $project['project_support']['browse_url'] = $url->toString();
        }
        else {
          $project['project_support']['status'] = 3;
        }
      }
      else {
        $project['project_support'] = $available_modules[$projectName];
        $url = Url::fromRoute('system.modules_list');
        $project['project_support']['install_url'] = $url->toString();
      }
      $module_handler = \Drupal::service('module_handler');
      $module_path = $module_handler->getModule('projects_browser')->getPath();
      global $base_url;
      $project_list_item = [
        '#theme' => 'project_list_item',
        '#project' => $project,
        '#terms' => $terms,
        '#empty_img' => $base_url . '/' . $module_path . '/images/empty-img.png',
      ];
      $projects[] = $project_list_item;
    }
    return $projects;
  }

  /**
   *
   */
  public function getProjectStatusData(string $projectName) {
    $available = NULL;
    $data = $this->updateFetcher->fetchProjectData($projectName);
    if (!empty($data)) {
      $available = $this->parseXml($data);
    }
    return $available;
  }

}
