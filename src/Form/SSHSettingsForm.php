<?php

namespace Drupal\projects_browser\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\FileTransfer\SSH;
use Drupal\Core\FileTransfer\FileTransferException;

/**
 * Provides terms overview form for a entity_taxonomy vocabulary.
 *
 * @internal
 */
class SSHSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'project-browser-ssh.settings';


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'projects_browser_ssh_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form['hostname'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('SSH Host:'),
      '#size' => 60,
      '#maxlength' => 200,
      '#required' => true,
      '#default_value' => $config->get('hostname'),
    );
    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('SSH Username:'),
      '#size' => 40,
      '#maxlength' => 60,
      '#required' => true,
      '#default_value' => $config->get('username'),
    );
    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('SSH Password:'),
      '#size' => 40,
      '#maxlength' => 60,
      '#default_value' => $config->get('password'),
    );
    $form['port'] = array(
      '#type' => 'number',
      '#title' => $this->t('SSH Port'),
      '#min' => 1,
      '#max' => 9999,
      '#required' => true,
      '#default_value' => $config->get('port'),
    );
    $form['composer_log'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Show Composer log'),
      '#min' => 1,
      '#max' => 9999,
      '#default_value' => $config->get('composer_log'),
    );
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => ['btn btn-last'],
      ],  
      '#value' =>  'Submit',
    ];
    return $form;
  }

  /**
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $hostname = $form_state->getValue('hostname');
    $username = $form_state->getValue('username');
    $password = $form_state->getValue('password');
    $port = $form_state->getValue('port');
    try {
      $ssh = new SSH(DRUPAL_ROOT, $username, $password, $hostname, $port);
      $ssh->connect();
    } catch (FileTransferException $e) {
      $message = $e->getMessage();
      if($message == 'Cannot connect to SSH Server, check settings') {
        $form_state->setErrorByName('hostname', $this->t($message));
      }
      elseif($message == "Cannot log in to SSH server. Check username and password") {
        $form_state->setErrorByName('username', $this->t($message));
      }
    }
  }

  /**
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $conf_keys = [
      'hostname',
      'username',
      'password',
      'port',
      'composer_log',
    ];
    $values = $form_state->getValues();
    $config = $this->config(static::SETTINGS);
    foreach ($conf_keys as $key) {
      if (isset($values[$key])) {
        $config->set($key, $values[$key]);
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }
}