<?php

namespace Drupal\projects_browser\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\projects_browser\Controller\ProjectDataController;
use Drupal\projects_browser\ProjectBrowserTrait;

/**
 * Provides terms overview form for a entity_taxonomy vocabulary.
 *
 * @internal
 */
class SearchProjectModulesForm extends FormBase {
  use ProjectBrowserTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'projects_browser_modules_form';
  }

  /**
   * Form constructor.
   *
   * Display a tree of all the terms in a vocabulary, with options to edit
   * each one. The form is made drag and drop by the theme function.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\entity_taxonomy\VocabularyInterface $entity_taxonomy_vocabulary
   *   The vocabulary to display the overview form for.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filters=[
      'type' => 'project_module',
      'sort' => 'title',
      'direction' => 'ASC'
    ];
    $projectListCallback =  [
      'callback' => '::projectListCallback', 
      'event' => 'change',
      'wrapper' => 'projects_list',
      'progress' => [
        'type' => 'projectbrowser',
        'message' => $this->t('fetching projects...'),
      ],
    ];
    $projectListCallback1 =  [
      'callback' => '::projectListCallback', 
      'event' => 'click',
      'wrapper' => 'projects_list',
      'progress' => [
        'type' => 'projectbrowser',
        'message' => $this->t('fetching projects...'),
      ],
    ];
    $form['#attributes'] = [
        'class' => 'projects-browser-form',
    ];
    $form['filters'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => 'filters form--inline',
      ),
    );
    $form['filters']['field_project_machine_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Project Machine Name'),
      '#size' => 30,
      '#maxlength' => 60,
      '#ajax' => $projectListCallback,
    );
    $form['filters']['taxonomy_vocabulary_44'] = [
      '#type' => 'select',
      '#title' => $this->t('Maintenance status'),
      '#options' => $this->getProjectBrowserTermOptions(44),
      '#ajax' => $projectListCallback,
    ];
    $form['filters']['taxonomy_vocabulary_46'] = [
      '#type' => 'select',
      '#title' => $this->t('Development status'),
      '#options' => $this->getProjectBrowserTermOptions(46),
      '#ajax' => $projectListCallback,
    ];
    $form['filters']['field_security_advisory_coverage'] = [
      '#type' => 'select',
      '#title' => $this->t('Security Advisory'),
      '#options' => [
        'any' => 'Any',
        'covered' => 'Covered',
        'not-covered' => 'Not Covered',
      ],
      '#ajax' => $projectListCallback,
    ];
    $form['filters']['taxonomy_vocabulary_3'] = [
      '#type' => 'select',
      '#title' => $this->t('Module categories'),
      '#options' => $this->getProjectBrowserTermOptions(3), 
      '#states' => array(
        'visible' => array(
          'select[name="type"]' => array(
            'value' => 'project_module',
          ),
        ),
      ),
      '#ajax' => $projectListCallback,
    ];

    $form['projectlists'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="projects_list">',
      '#suffix' => '</div>',
    );
    $pdc = new ProjectDataController();
    $project_data = $pdc->getProjects($filters);
    $form['projectlists']['content'] = $project_data['content'];
    $form['projectlists']['bottom-filters'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'bottom-filters',
      ],
    ];
    $form['projectlists']['bottom-filters']['sort-filters'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'sort-filters',
      ],  
    ];
    $form['projectlists']['bottom-filters']['sort-filters']['sort'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort by'),
      '#options' => [
        'title' => 'Project Title',
        'changed' => 'Modified',
        'created' => 'Created',
        'taxonomy_vocabulary_44' => 'Maintenance status',
        'taxonomy_vocabulary_46' => 'Development Status',
      ],
      '#ajax' => $projectListCallback,
    ];
    $form['projectlists']['bottom-filters']['sort-filters']['direction'] = [
      '#type' => 'select',
      '#title' => $this->t('Order'),
      '#options' => [
        'ASC' => 'Ascending',
        'DESC' => 'Descending',
      ],
      '#ajax' => $projectListCallback,
    ];
    $form['projectlists']['bottom-filters']['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'pager-container',
      ],  
    ];
    $form['projectlists']['bottom-filters']['actions']['pager_first'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => ['btn btn-first'],
      ],
      '#value' => 'First',
      '#ajax' => $projectListCallback1,
    ];
    $form['projectlists']['bottom-filters']['actions']['pager_prev'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => ['btn btn-prev'],
      ],
      '#value' => 'Previous',
      '#ajax' => $projectListCallback1,
    ];
    $form['projectlists']['bottom-filters']['actions']['pager_self'] = [
      '#type' => 'number',
      '#size' => 5,
      '#min' => 0,
      '#max' => 1,
      '#maxlength' => 5,
      '#ajax' => $projectListCallback,
    ];
    $form['projectlists']['bottom-filters']['actions']['pager_next'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => ['btn btn-next'],
      ],  
      '#value' => 'Next',
      '#ajax' => $projectListCallback1,
    ];
    $form['projectlists']['bottom-filters']['actions']['pager_last'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => ['btn btn-last'],
      ],  
      '#value' =>  'Last',
      '#ajax' => $projectListCallback1,
    ];
    if(isset($project_data['pager']['last'])){     
      $form['projectlists']['bottom-filters']['actions']['pager_self']['#default_value'] = (int)$project_data['pager']['self'];
      $form['projectlists']['bottom-filters']['actions']['pager_self']['#suffix'] = $this->t(' of ') . $project_data['pager']['last'];
      $form['projectlists']['bottom-filters']['actions']['pager_self']['#max'] = (int)$project_data['pager']['last'];
    }
    return $form;
  }

  public function projectListCallback(array &$form, FormStateInterface $form_state) {
    $filters=[
      'type' => 'project_module',
    ];
    if ($sort = $form_state->getValue('field_project_machine_name')) {
          $filters['field_project_machine_name'] = $sort;
    }
    if ($selectedValue = $form_state->getValue('field_project_type')) {
        if($selectedValue!= 'any')
          $filters['field_project_type'] = $selectedValue;
    }
    if ($selectedValue = $form_state->getValue('taxonomy_vocabulary_46')) {
        if($selectedValue!= 'any')
          $filters['taxonomy_vocabulary_46'] = $selectedValue;
    }
    if ($selectedValue = $form_state->getValue('taxonomy_vocabulary_44')) {
        if($selectedValue!= 'any')
          $filters['taxonomy_vocabulary_44'] = $selectedValue;
    }
    if ($selectedValue = $form_state->getValue('taxonomy_vocabulary_3')) {
        if($selectedValue!= 'any')
          $filters['taxonomy_vocabulary_3'] = $selectedValue;
        $selectedText = $form['taxonomy_vocabulary_3']['#options'][$selectedValue];
        $form['projectlists']['data2'] = [
          '#markup' => $selectedText,
        ];
    }
    if ($sort = $form_state->getValue('sort')) {
          $filters['sort'] = $sort;
    }
    if ($order = $form_state->getValue('direction')) {
          $filters['direction'] = $order;
    }
    if ($pager = $form_state->getValue('op')) {
          $filters['pager'] = $pager;
    }
    if ($pager = $form_state->getValue('pager_self')) {
          $filters['pager_self'] = $pager;
    }
    if ($selectedValue = $form_state->getValue('field_security_advisory_coverage')) {
      if($selectedValue!= 'any')
          $filters['field_security_advisory_coverage'] = $selectedValue;
    }

    $pdc = new ProjectDataController();
    $project_data = $pdc->getProjects($filters);
    $form['projectlists']['content'] = $project_data['content'];
    if(isset($project_data['pager']['last'])) {
      $form['projectlists']['bottom-filters']['actions']['pager_self']['#value'] = (int)$project_data['pager']['self'];
      $form['projectlists']['bottom-filters']['actions']['pager_self']['#suffix'] = $this->t(' of ') . $project_data['pager']['last'];
      $form['projectlists']['bottom-filters']['actions']['pager_self']['#max'] = (int)$project_data['pager']['last'];
    }
    return $form['projectlists']; 
  }

  /**
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
}
