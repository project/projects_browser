<?php

namespace Drupal\projects_browser;

use Composer\Semver\Semver;
use Drupal\Core\FileTransfer\SSH;
use \Drupal\Core\Url;
use Drupal\Core\FileTransfer\FileTransferException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a trait for the messenger service.
 */
trait ProjectBrowserTrait {

  /**
   * Gets the module status list.
   *
   * @return module_status
   *   The module status list.
   */
  protected function getModuleStatusList() {
    $moduleExtensionList = \Drupal::service('extension.list.module');
    $modules = $moduleExtensionList->reset()->getList();
    $module_status = [];
    foreach ($modules as $name => $module) {
      $module_status[$name] = [
        'name' => $module->info['name'],
        'version' => isset($module->info['version']) ? $module->info['version'] : '',
        'status' => $module->status,
      ];
    }
    return $module_status;
  }

  /**
   *
   */
  protected function getUrlParameters($url) {
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    return $query;
  }

  /**
   *
   */
  protected function parseXml($raw_xml) {
    try {
      $xml = new \SimpleXMLElement($raw_xml);
    }
    catch (\Exception $e) {
      // SimpleXMLElement::__construct produces an E_WARNING error message for
      // each error found in the XML data and throws an exception if errors
      // were detected. Catch any exception and return failure (NULL).
      return NULL;
    }
    // If there is no valid project data, the XML is invalid, so return failure.
    if (!isset($xml->short_name)) {
      return NULL;
    }
    $data = [];
    foreach ($xml as $k => $v) {
      $data[$k] = (string) $v;
    }
    $data['is_compatible'] = 0;
    if (isset($xml->releases)) {
      foreach ($xml->releases->children() as $release) {
        if ($release->core_compatibility != '' && Semver::satisfies(\Drupal::VERSION, $release->core_compatibility)) {
          $version = (string) $release->version;
          $version = str_replace('8.x-', '', $version);
          $version = str_replace('1.x-', '', $version);
          $version = str_replace('2.x-', '', $version);
          if ($release->security == "Covered by Drupal's security advisory policy") {
            $data['is_compatible'] = 1;
            $data['compatible_versions'] = $version;
            break;
          }
          elseif ($data['is_compatible'] == 0) {
            $data['is_compatible'] = 1;
            $data['compatible_versions'] = $version;
          }
        }
      }
    }
    return $data;
  }

  /**
   *
   */
  public function getProjectBrowserTerms() {
    $database = \Drupal::database();
    $query = $database->select('projects_browser_term_data', 'pbt');
    $query->fields('pbt', ['tid', 'name']);
    $results = $query->execute();
    while ($result = $results->fetchAssoc()) {
      $options[$result['tid']] = $result['name'];
    }
    return $options;
  }

  /**
   *
   */
  public function getProjectBrowserTermOptions($vid) {
    $database = \Drupal::database();
    $query = $database->select('projects_browser_term_data', 'pbt');
    $query->fields('pbt', ['tid', 'name']);
    $query->condition('vid', $vid);
    $results = $query->execute();
    $options = [
      'any' => t('All'),
    ];
    while ($result = $results->fetchAssoc()) {
      $options[$result['tid']] = $result['name'];
    }
    return $options;
  }

  /**
   *
   */
  protected function getSshClient() {
    try {
      $config = \Drupal::config('project-browser-ssh.settings');
      $hostname = $config->get('hostname');
      $username = $config->get('username');
      $password = $config->get('password');
      $port = $config->get('port');
      $ssh = new SSH(DRUPAL_ROOT, $username, $password, $hostname, $port);
      $ssh->connect();
      return $ssh;
    }
    catch (FileTransferException $e) {
      $message = $e->getMessage();
      \Drupal::messenger()->addMessage($message);
      $url = Url::fromRoute('projects_browser.ssh_settings')->toString();
      $response = new RedirectResponse($url);
      $response->send();
    }
    return NULL;
  }

}
