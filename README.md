# Projects Browser

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Configuration

INTRODUCTION
------------

This module provides browsing projects from drupal.org & The module browsed by composer with SSH

REQUIREMENTS
------------

 * This module requires php-xml and php-ssh2 extension & need SSH support from webserver.

CONFIGURATION
-------------

 * Configure your SSH credentials
    ```sh
    /admin/config/projects-browser/settings
    ```
