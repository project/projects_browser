(function ($, Drupal) {
  Drupal.Ajax.prototype.setProgressIndicatorProjectbrowser = function () {
    this.progress.element = $(Drupal.theme('ajaxProgressProjectbrowser', this.progress.message));
    $(this.element).after(this.progress.element);
  };

  Drupal.theme.ajaxProgressProjectbrowser = function (message) {
    var messageMarkup = typeof message === 'string' ? Drupal.theme('ajaxProgressMessage', message) : '';
    var throbber = '<div class="ajax-project-browser">&nbsp;</div>';
    return "<div class=\"project-browser-overlay\"><div class=\"ajax-progress ajax-progress-project-browser\"><div class=\"loader\"></div>".concat(throbber).concat(messageMarkup, "</div></div>");
  };

})(jQuery, Drupal);
